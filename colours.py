def red(string):
    return "\033[1;91m"+string+"\033[0m"


def green(string):
    return "\033[1;92m"+string+"\033[0m"


def yellow(string):
    return "\033[1;93m"+string+"\033[0m"


def blue(string):
    return "\033[1;94m"+string+"\033[0m"


def bold(string):
    return "\033[1m"+string+"\033[0m"


if __name__ == "__main__":
    print("Please run the script ktane.py instead!")
